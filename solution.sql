Find all artists that has letter D in its name:
	SELECT * FROM artists WHERE name LIKE '%D%';

Find all songs that has a length of less than 230:
	SELECT * FROM songs WHERE length < 230;

Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length:
	SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id=songs.album_id;

Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name:
	SELECT * FROM artists JOIN albums ON artists.id=albums.artist_id WHERE album_title LIKE '%A%';

Sort the albums in Z-A order. (Show only the first 4 records:
	SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z:
	SELECT * FROM albums JOIN songs ON albums.id=songs.album_id ORDER BY album_title DESC;
	SELECT * FROM albums JOIN songs ON albums.id=songs.album_id ORDER BY song_name ASC;